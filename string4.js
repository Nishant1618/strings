function string4(ob){
    if (!ob){
        return []
    }else {
        let res = Object.values(ob)
    for (let i = 0; i < res.length; i++){
        res[i] = res[i].toLowerCase()
    }
    for (let i = 0; i < res.length; i++) {
        res[i] = res[i].charAt(0).toUpperCase() + res[i].slice(1); 
  }return res.join(' ');
    }   
} 

module.exports = string4;
